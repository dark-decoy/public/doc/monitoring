* [Tuning NextCloud for Optimal Performance \- Autoize](https://autoize.com/tuning-nextcloud-performance/ "Tuning NextCloud for Optimal Performance - Autoize")
* [Resources \| Gatling](https://gatling.io/resources/ "Resources | Gatling")
* [Performance Test for Nextcloud \: r\/NextCloud](https://www.reddit.com/r/NextCloud/comments/sjtcji/performance_test_for_nextcloud/ "Performance Test for Nextcloud : r/NextCloud")
* [404 Page not found \- Gatling Documentation](https://docs.gatling.io/reference/install/self-hosted/ansible/ "404 Page not found - Gatling Documentation")
