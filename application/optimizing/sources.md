* [An Introduction to PHP\-FPM Tuning – Tideways](https://tideways.com/profiler/blog/an-introduction-to-php-fpm-tuning "An Introduction to PHP-FPM Tuning – Tideways")
* [Get started with Bootstrap · Bootstrap v5\.3](https://getbootstrap.com/docs/5.3/getting-started/introduction/ "Get started with Bootstrap · Bootstrap v5.3")
* [rsync oneliner\: a study of a complex commandline \- anarcat](https://anarc.at/blog/2019-07-07-rsync-oneliner/ "rsync oneliner: a study of a complex commandline - anarcat")
* [Wesley Aptekar\-Cassels \| End\-to\-end testing emails](https://blog.wesleyac.com/posts/e2e-testing-email "Wesley Aptekar-Cassels | End-to-end testing emails")
